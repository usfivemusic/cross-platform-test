// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.CodeDom.Compiler;

namespace iOSClassic
{
	[Register ("iOS_ClassicViewController")]
	partial class iOS_ClassicViewController
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton btnEcho { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lblResult { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITextField txtInput { get; set; }

		[Action ("btnEcho_TouchUpInside:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void btnEcho_TouchUpInside (UIButton sender);

		void ReleaseDesignerOutlets ()
		{
			if (btnEcho != null) {
				btnEcho.Dispose ();
				btnEcho = null;
			}
			if (lblResult != null) {
				lblResult.Dispose ();
				lblResult = null;
			}
			if (txtInput != null) {
				txtInput.Dispose ();
				txtInput = null;
			}
		}
	}
}
