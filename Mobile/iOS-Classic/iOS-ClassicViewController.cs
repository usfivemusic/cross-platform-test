﻿using System;
using System.Drawing;
using Core;

using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace iOSClassic
{
	public partial class iOS_ClassicViewController : UIViewController
	{
		private Echoer _echoer;

		static bool UserInterfaceIdiomIsPhone {
			get { return UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone; }
		}

		public iOS_ClassicViewController (IntPtr handle) : base (handle)
		{
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}
        
        partial void btnEcho_TouchUpInside(UIButton sender)
        {
            var inputText = txtInput.Text;
            var echoResult = _echoer.Echo(inputText);
            lblResult.Text = echoResult;
        }

		#region View lifecycle

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			
			// Perform any additional setup after loading the view, typically from a nib.
			_echoer = new Echoer ();
		}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
		}

		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);
		}

		public override void ViewWillDisappear (bool animated)
		{
			base.ViewWillDisappear (animated);
		}

		public override void ViewDidDisappear (bool animated)
		{
			base.ViewDidDisappear (animated);
		}


		
		#endregion
	}
}

