﻿using System;
using Core;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

namespace Android
{
	[Activity (Label = "Android", MainLauncher = true, Icon = "@drawable/icon")]
	public class MainActivity : Activity
	{
		//int count = 1;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			// Set our view from the "main" layout resource
			SetContentView (Resource.Layout.Main);
		
			var btnEcho = FindViewById<Button> (Resource.Id.btnEcho);
			var txtInput = FindViewById<EditText> (Resource.Id.txtInput);
			var txtResult = FindViewById<EditText> (Resource.Id.txtResult);

			var echoer = new Echoer ();

			btnEcho.Click += (object sender, EventArgs e) => {
				var inputText = txtInput.Text;
				var echoResult = echoer.Echo(inputText);
				txtResult.Text = echoResult;
			};
		}
	}
}


