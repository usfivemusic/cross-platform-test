// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace Mac
{
	[Register ("MainWindowController")]
	partial class MainWindowController
	{
		[Outlet]
		AppKit.NSButtonCell btnEcho { get; set; }

		[Outlet]
		AppKit.NSTextField lblResult { get; set; }

		[Outlet]
		AppKit.NSTextField txtInput { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (btnEcho != null) {
				btnEcho.Dispose ();
				btnEcho = null;
			}

			if (txtInput != null) {
				txtInput.Dispose ();
				txtInput = null;
			}

			if (lblResult != null) {
				lblResult.Dispose ();
				lblResult = null;
			}
		}
	}
}
