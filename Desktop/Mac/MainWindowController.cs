﻿using System;
using Core;

using Foundation;
using AppKit;

namespace Mac
{
	public partial class MainWindowController : NSWindowController
	{
		private Echoer _echoer;

		public MainWindowController (IntPtr handle) : base (handle)
		{
		}

		[Export ("initWithCoder:")]
		public MainWindowController (NSCoder coder) : base (coder)
		{
		}

		public MainWindowController () : base ("MainWindow")
		{
		}

		public override void AwakeFromNib ()
		{
			base.AwakeFromNib ();

			_echoer = new Echoer ();

			btnEcho.Activated += (object sender, EventArgs e) => {
				var inputText = txtInput.StringValue;

				var echoResult = _echoer.Echo(inputText);

				lblResult.StringValue = echoResult;
			};
		}

		public new MainWindow Window {
			get { return (MainWindow)base.Window; }
		}
	}
}
