﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Core;

namespace Win_WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Echoer _echoer;

        public MainWindow()
        {
            InitializeComponent();

            _echoer = new Echoer();
        }

        private void btnEcho_Click(object sender, RoutedEventArgs e)
        {
            var inputText = txtInput.Text;
            var echoResult = _echoer.Echo(inputText);
            lblResult.Content = echoResult;
        }


    }
}
